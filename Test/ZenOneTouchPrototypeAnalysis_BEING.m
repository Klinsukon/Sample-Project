clear all, close all
%=== Initial parameters ===%
samplingRate = 83;
nBit = 15;
% Path of RawData
% Example: pathFolder ='D:\srisuda\Improve - AKE\Application\';
pathFolder='F:\Ake Zen\MATLAB\Improve - AKE\Application\';
%=== List the folders as this path 
folderlist = dir(pathFolder);
folderlist = {folderlist.name}.';

%=== Create the results folder
% Example: pathResult='D:\srisuda\Improve - AKE\Result\AKE_TEST\';
pathResult='F:\Ake Zen\MATLAB\Improve - AKE\Result\20150430\DC\';
mkdir(pathResult)
%=== Set folder name for save the result
chkPeakDetection = 'PeakDetection\';% The results are recorded into this floder
filterfolder = 'FilterResult\';
chkSignal = 'chkSignal\';
chkPeakdet = 'chkPeakdet\';
chkFingerAndTuning = 'chkFingerAndTuning\';
for folder = 7%:length(folderlist)
    pathFile = [pathFolder  folderlist{folder,1} '\'];
    %=== List the .csv file as in this path
    filelist = ls([pathFile '*.csv']);
    % filelist = 'Somchai_37_RawDataOn_20141215112452.csv';
    filelist = cellstr(filelist);
    
    for nfile = 1 : length(filelist)
        %=== Extracted the data from raw data. 
        %[Age,subject,serial,sheetname,Flag,Packet,ACIR,DCIR,ACR,DCR,CaliSBP,CaliDBP] = funPrepareInputData('Application',pathFile,filelist{nfile});
        [Age,subject,serial,sheetname,Flag,Packet,ACIR,DCIR,...
        ACRED,DCRED,CaliSBP,CaliDBP,AppRT,AppHR,AppSpO2,AppSBP,AppDBP,...
        AppOffsetSBP,AppOffsetDBP,AppeSBP,AppeDBP,AppQM] = funPrepareInputData2('FLUKE',pathFile,filelist{nfile},nBit);

            %=== Check the raw data file.
            % Due to raw data from application have 3 files (RAW,RESULT,and N/A)
            % We use 2 files, RAW file collected the all output from firmware and
            % RESULT file collected the results are computed by application.
            if ~isempty(subject)% Check file.
                %=== Check Packet Data Lost all subjects
                newPacketLostReport = funChkPacketDataLost(subject,serial,Flag,Packet);
                nnewPacket = size(newPacketLostReport,2);
                if exist('PacketLostReport')==0;
                    PacketLostReport = newPacketLostReport;
                else
                    nPacket = size(PacketLostReport,2);
                    if nPacket == nnewPacket
                        PacketLostReport = [PacketLostReport;newPacketLostReport];
                    elseif nnewPacket > nPacket
                        fillZeros = zeros(size(PacketLostReport,1),nnewPacket - nPacket);
                        PacketLostReport =[PacketLostReport num2cell(fillZeros)];
                        PacketLostReport = [PacketLostReport;newPacketLostReport];
                    elseif nnewPacket < nPacket
                        fillZeros = zeros(1,nPacket - nnewPacket);
                        newPacketLostReport =[newPacketLostReport num2cell(fillZeros)];
                        PacketLostReport = [PacketLostReport;newPacketLostReport];
                    end
                end
                close
                %=== End for checking packet data

                %=== Load coefficient for FIR filter===%
                load FiltCoeff.mat
                FiltCoeff = Filt128Coeff;

                %=== Filter flag 41 for ACRED and ACIR ===%
                %=== IR signal ===%
                idFlag41 = find(Flag==41);
                idFlag11 = find(Flag==11);
                idFlag22 = find(Flag==22);
                copyDCIR = DCIR(idFlag41,1);
                copyDCRED = DCRED(idFlag41,1);
                copyACIR = ACIR(idFlag41,1);
                copyACRED = ACRED(idFlag41,1);
                FingerDetIR = DCIR(idFlag11,1);
                FingerDetRED = DCRED(idFlag11,1);
                TuningIR = DCIR(idFlag22,1);
                TuningRED = DCRED(idFlag22,1);
                %=== FFT
                funFFT(copyDCIR,copyACIR,copyDCRED,copyACRED,samplingRate,sheetname,pathResult)
                 % Ake Commend
                %===Check Finger detec&Tuning phase ===%
                          
                figure,
                subplot(2,1,1);hold on
                plot(FingerDetIR,'b','linewidth',2);
                plot(FingerDetRED,'r','linewidth',2);
                ylabel('Flag 11')
                title(sheetname)
                subplot(2,1,2);hold on
                plot(TuningIR,'b','linewidth',2);
                plot(TuningRED,'r','linewidth',2);
                ylabel('Flag 22')
                %==== Full screen
                set(gcf,'Visible','on');
                ScrSize = get(0,'ScreenSize');
                set(gcf,'Units','pixels','Position',ScrSize);
                %===== Export Image
                paththisResult = [pathResult chkFingerAndTuning];
                mkdir(paththisResult);% Create new folder
        
                savepath = [paththisResult sheetname '.jpg'];
                hgexport(gcf,savepath,hgexport('factorystyle'), 'Format', 'jpeg');
                %===============================
                % End Ake Commend

                %=== Initail parameters ===%
                SampleNo = length(idFlag41);

                %=== Find 1st order IIR Low Pass Filter @ Fc = 0.5 Hz ===%
                irLPF = funIIRlowpass(copyACIR,samplingRate,0.5);
                rLPF = funIIRlowpass(copyACRED,samplingRate,0.5);
                %********************************************************%
                %=============== copyACIR --> copyDCIR ==================%
                %================ copyACR --> copyDCR ===================%
                irBPF_ADC2 = funFIR(copyACIR,FiltCoeff,128);
                rBPF_ADC2 = funFIR(copyACRED,FiltCoeff,128);
                irBPF_ADC1 = funFIR(copyDCIR,FiltCoeff,128);
                rBPF_ADC1 = funFIR(copyDCRED,FiltCoeff,128);
                %=== Signal to Noise ratio
%                 [SNRratio] = funSNR(RawSiganl,FilterSignal,nameSignal,Fs,pathResult,sheetname)
                %=== ignore the first 2sec and the final 2sec
                upperLimit = SampleNo-2*samplingRate;%SampleNo - (samplingRate * 2);
                lowerLimit = 2*samplingRate;%samplingRate * 2-2;
                
                irBPF_ADC2 = irBPF_ADC2(lowerLimit:upperLimit,1);
                rBPF_ADC2 = rBPF_ADC2(lowerLimit:upperLimit,1);
                irBPF_ADC1 = irBPF_ADC1(lowerLimit:upperLimit,1);
                rBPF_ADC1 = rBPF_ADC1(lowerLimit:upperLimit,1);
                
                irLPF = irLPF(lowerLimit:upperLimit,1);
                rLPF = rLPF(lowerLimit:upperLimit,1);
                %=== Peak and Valley Detection ===%
                %=== IR 
                [MaxPtsFingerIR, MinPtsFingerIR] = funFindMinMaxSlope2(irBPF_ADC2,samplingRate);
                PeakPosIR = MaxPtsFingerIR(:,1);
                PeakValIR = irBPF_ADC2(PeakPosIR,1);
                ValleyPosIR = MinPtsFingerIR(:,1);
                ValleyValIR = irBPF_ADC2(ValleyPosIR,1);
                %=== PPG Features ===%
                [Rratio1pulse,AmpIR1Pulse,Tt1Pulse_ValleyPos,Tt1Pulse_PeakPos,...
                Tr1Pulse,Tf1Pulse,TrTf1Pulse,RT1Pulse]= funFeaturesPPG(samplingRate,...
                irBPF_ADC2,rBPF_ADC2,irLPF,rLPF,ValleyPosIR,PeakPosIR);
                %=== Statistic Features ===% 
                [allSD_Rratio,Rratio,selSD_Rratio,idSelected_Rratio] = funFeaturesStatistic(Rratio1pulse);
                [allSD_AmpIR,AmpIR,selSD_AmpIR,idSelected_AmpIR] = funFeaturesStatistic(AmpIR1Pulse);
                [allSD_TtValleyPos,TtValleyPos,selSD_TtValleyPos,idSelected_TtValleyPos] = funFeaturesStatistic(Tt1Pulse_ValleyPos);
                [allSD_TtPeakPos,TtPeakPos,selSD_TtPeakPos,idSelected_TtPeakPos] = funFeaturesStatistic(Tt1Pulse_PeakPos);
                [allSD_Tr,Tr,selSD_Tr,idSelected_Tr] = funFeaturesStatistic(Tr1Pulse);
                [allSD_Tf,Tf,selSD_Tf,idSelected_Tf] = funFeaturesStatistic(Tf1Pulse);
                [allSD_TrTfratio,TrTfratio,selSD_TrTfratio,idSelected_TrTfratio] = funFeaturesStatistic(TrTf1Pulse);
                [allSD_RT,RT,selSD_RT,idSelected_RT] = funFeaturesStatistic(RT1Pulse);
                
                %=== Chk Signal Quality
                chkidSelected = diff(diff([idSelected_TtValleyPos;idSelected_TtPeakPos;idSelected_Tf]));
                chkidSelected = length(find(chkidSelected~=0));
                Th1 = 0.04;
                Th2 = 0.1;
                [SignalStatus] = funChksignal(selSD_TtValleyPos,selSD_TtPeakPos,selSD_Tf,Th1,Th2);
                
                % Ake Commend
%                 title(sheetname)
%                 %==== Full screen
%                 set(gcf,'Visible','on');
%                 ScrSize = get(0,'ScreenSize');
%                 set(gcf,'Units','pixels','Position',ScrSize);
%                 %===== Export Image
%                 paththisResult = [pathResult chkPeakdet];
%                 mkdir(paththisResult);% Create new folder
%         
%                 savepath = [paththisResult sheetname '.jpg'];
%                 hgexport(gcf,savepath,hgexport('factorystyle'), 'Format', 'jpeg');
                %===============================
                % End Ake Commend
                
                %===PeakDetection 
                figure(2),subplot(2,1,1)
                hold on
                %==== Plot ACIR
                plot(1:length(irBPF_ADC2),irBPF_ADC2,'linewidth',2,'color','b');
                %==== Plot peak IR
                plot(PeakPosIR,PeakValIR,'.m','markersize',14);

                for nPeak = 1:length(PeakPosIR)-1
                    text(PeakPosIR(nPeak),PeakValIR(nPeak),mat2str(PeakPosIR(nPeak)))
                end
                %==== Plot Valley IR
                plot(ValleyPosIR,ValleyValIR,'.g','markersize',14);

                for nValley= 1:length(ValleyPosIR)
                    text(ValleyPosIR(nValley),ValleyValIR(nValley),mat2str(ValleyPosIR(nValley)))
                end
                
                txtAmpIR = round((AmpIR*100))/100;
                txtTrTfratio = round((TrTfratio*100))/100;
                
%                 ylim([-(2.^nBit)/2 (2.^nBit)/2])
                ylabel('ACIR (ADC)');
                title([sheetname '  Amp = ' mat2str(txtAmpIR) ',  TrTf = ' mat2str(txtTrTfratio) ', SQ = ' SignalStatus],'FontSize',14);
                %==== ADC1
                [MaxPtsFingerIR, MinPtsFingerIR] = funFindMinMaxSlopeParameters(irBPF_ADC1,samplingRate);
                PeakPosIR = MaxPtsFingerIR(:,1);
                PeakValIR = irBPF_ADC1(PeakPosIR,1);
                ValleyPosIR = MinPtsFingerIR(:,1);
                ValleyValIR = irBPF_ADC1(ValleyPosIR,1);
                %=== PPG Features ===%
                [Rratio1pulse,AmpIR1Pulse,Tt1Pulse_ValleyPos,Tt1Pulse_PeakPos,...
                Tr1Pulse,Tf1Pulse,TrTf1Pulse,RT1Pulse]= funFeaturesPPG(samplingRate,...
                irBPF_ADC1,rBPF_ADC1,irLPF,rLPF,ValleyPosIR,PeakPosIR);
                %=== Statistic Features ===% 
                [allSD_Rratio,Rratio,selSD_Rratio,idSelected_Rratio] = funFeaturesStatistic(Rratio1pulse);
                [allSD_AmpIR,AmpIR,selSD_AmpIR,idSelected_AmpIR] = funFeaturesStatistic(AmpIR1Pulse);
                [allSD_TtValleyPos,TtValleyPos,selSD_TtValleyPos,idSelected_TtValleyPos] = funFeaturesStatistic(Tt1Pulse_ValleyPos);
                [allSD_TtPeakPos,TtPeakPos,selSD_TtPeakPos,idSelected_TtPeakPos] = funFeaturesStatistic(Tt1Pulse_PeakPos);
                [allSD_Tr,Tr,selSD_Tr,idSelected_Tr] = funFeaturesStatistic(Tr1Pulse);
                [allSD_Tf,Tf,selSD_Tf,idSelected_Tf] = funFeaturesStatistic(Tf1Pulse);
                [allSD_TrTfratio,TrTfratio,selSD_TrTfratio,idSelected_TrTfratio] = funFeaturesStatistic(TrTf1Pulse);
                [allSD_RT,RT,selSD_RT,idSelected_RT] = funFeaturesStatistic(RT1Pulse);
                %=== Chk Signal Quality
                chkidSelected = diff(diff([idSelected_TtValleyPos;idSelected_TtPeakPos;idSelected_Tf]));
                chkidSelected = length(find(chkidSelected~=0));
                Th1 = 0.04;
                Th2 = 0.1;
                [SignalStatus] = funChksignal(selSD_TtValleyPos,selSD_TtPeakPos,selSD_Tf,Th1,Th2);
                %===PeakDetection 
                figure(2),subplot(2,1,2)
                hold on
                %==== Plot ACIR
                plot(1:length(irBPF_ADC1),irBPF_ADC1,'linewidth',2,'color','b');
                %==== Plot peak IR
                plot(PeakPosIR,PeakValIR,'.m','markersize',14);

                for nPeak = 1:length(PeakPosIR)-1
                    text(PeakPosIR(nPeak),PeakValIR(nPeak),mat2str(PeakPosIR(nPeak)))
                end
                %==== Plot Valley IR
                plot(ValleyPosIR,ValleyValIR,'.g','markersize',14);

                for nValley= 1:length(ValleyPosIR)
                    text(ValleyPosIR(nValley),ValleyValIR(nValley),mat2str(ValleyPosIR(nValley)))
                end
                
                txtAmpIR = round((AmpIR*100))/100;
                txtTrTfratio = round((TrTfratio*100))/100;
                
%                 ylim([-((2.^nBit)/10)/2 ((2.^nBit)/10)/2])
                ylabel('DCIR (ADC)');
                title([sheetname '  Amp = ' mat2str(txtAmpIR) ',  TrTf = ' mat2str(txtTrTfratio) ', SQ = ' SignalStatus],'FontSize',14);
                
                %==== Full screen
                set(gcf,'Visible','on');
                ScrSize = get(0,'ScreenSize');
                set(gcf,'Units','pixels','Position',ScrSize);
                %===== Export Image
                paththisResult = [pathResult chkPeakDetection];
                mkdir(paththisResult);% Create new folder                
                savepath = [paththisResult sheetname '.jpg'];
                hgexport(gcf,savepath,hgexport('factorystyle'), 'Format', 'jpeg') 
                
                %=== Report chkSignal  
                funSignalResult(nBit,irBPF_ADC2,rBPF_ADC2,irLPF,rLPF,copyDCIR,...
                copyDCRED,copyACIR,copyACRED,sheetname,PeakPosIR,PeakValIR,...
                ValleyPosIR,ValleyValIR) 

                %==== Full screen
                set(gcf,'Visible','on');
                ScrSize = get(0,'ScreenSize');
                set(gcf,'Units','pixels','Position',ScrSize);
                %===== Export Image
                paththisResult = [pathResult chkSignal];
                mkdir(paththisResult);% Create new folder

                savepath = [paththisResult sheetname '.jpg'];
                hgexport(gcf,savepath,hgexport('factorystyle'), 'Format', 'jpeg');
                %===============================
                %=== Calculated Zen OneTouch parameters ===%
%                 [HR] = funHR(ValleyPosIR,samplingRate);
% 
%                 [SpO2,Rratio] = funSpO2(irBPF,rBPF,irLPF,rLPF,ValleyPosIR,PeakPosIR,ValleyPosIR,PeakPosIR);
%                 [SBP,DBP] = funBP(PeakPosIR,ValleyPosIR,samplingRate,CaliSBP,CaliDBP,subject);
            end% If loop for check raw data 
        close all
    end% For loop of filelist 
end% For loop of folderlist

'=== Finish ==='